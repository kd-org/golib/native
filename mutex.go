// https://h12.io/article/go-pattern-context-aware-lock
package native

import "context"

type ContextLocker interface {
	Lock(ctx context.Context) bool
	Unlock()
	Locked() bool
}

func ContextMutex() ContextLocker {
	return &ctxMutex{make(chan struct{}, 1)}
}

type ctxMutex struct {
	ch chan struct{}
}

func (mu *ctxMutex) Lock(ctx context.Context) bool {
	select {
	case <-ctx.Done():
		return false
	case mu.ch <- struct{}{}:
		return true
	}
}

func (mu *ctxMutex) Unlock() {
	<-mu.ch
}

func (mu *ctxMutex) Locked() bool {
	return len(mu.ch) > 0 // locked or not
}
