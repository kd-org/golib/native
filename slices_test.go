package native

import (
	"fmt"
	"testing"
)

func TestConcatSlices(t *testing.T) {
	output := ConcatSlices([]int{1, 2, 3}, []int{4, 5, 6}, []int{7, 8, 9})
	if len(output) != 9 {
		t.Fatalf("length of %v (%d) should be 9", output, len(output))
	}
}

func TestMapSlices(t *testing.T) {
	output := MapSlices(func(n int) string {
		return fmt.Sprintf("%d", n)
	}, []int{1, 2, 3}, []int{4, 5, 6}, []int{7, 8, 9})
	if len(output) != 9 {
		t.Fatalf("length of %v (%d) should be 9", output, len(output))
	}
	if output[1] != "2" {
		t.Fatalf("output[1] %v should be \"2\"", output[1])
	}
}
