package native

func Must2[T any](data T, err error) T {
	if err != nil {
		panic(err)
	}
	return data
}
