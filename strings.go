package native

// https://stackoverflow.com/questions/34839659/how-can-i-easily-get-a-substring-in-go-while-guarding-against-slice-bounds-out
func MaxString(s string, max int) string {
	if len(s) > max {
		r := 0
		for i := range s {
			r++
			if r > max {
				return s[:i]
			}
		}
	}
	return s
}
