package native

type Set[K comparable] map[K]struct{}

func (t Set[K]) Add(values ...K) {
	for _, value := range values {
		t[value] = struct{}{}
	}
}

func (t Set[K]) Has(value K) bool {
	_, ok := t[value]
	return ok
}

func (t Set[K]) HasAll(values []K) bool {
	for _, value := range values {
		if !t.Has(value) {
			return false
		}
	}
	return true
}

func (t Set[K]) HasAny(values []K) bool {
	for _, value := range values {
		if t.Has(value) {
			return true
		}
	}
	return false
}

func (t Set[K]) Slice() []K {
	result := make([]K, 0, len(t))
	for value := range t {
		result = append(result, value)
	}
	return result
}

func NewSetFromSlice[K comparable](slices ...[]K) Set[K] {
	set := Set[K]{}
	for _, slice := range slices {
		for _, value := range slice {
			set[value] = struct{}{}
		}
	}
	return set
}

func NewSetFromSliceFn[T any, K comparable](getKey func(T) (K, bool), slices ...[]T) Set[K] {
	set := Set[K]{}
	for _, slice := range slices {
		for _, value := range slice {
			key, ok := getKey(value)
			if ok {
				set[key] = struct{}{}
			}
		}
	}
	return set
}
