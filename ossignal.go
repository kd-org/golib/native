package native

import (
	"os"
	"os/signal"
	"syscall"
)

func NewCloseSignal() <-chan os.Signal {
	osSignalClose := make(chan os.Signal, 1)
	signal.Notify(osSignalClose, os.Interrupt, syscall.SIGTERM)

	return osSignalClose
}
