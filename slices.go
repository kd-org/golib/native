package native

func ConcatSlices[T any](slices ...[]T) []T {
	var totalLen int
	for _, slice := range slices {
		totalLen += len(slice)
	}

	output := make([]T, 0, totalLen)
	for _, slice := range slices {
		output = append(output, slice...)
	}

	return output
}

func FilterSlices[T any](filter func(T) bool, slices ...[]T) []T {
	var output []T
	for _, slice := range slices {
		for _, value := range slice {
			if filter(value) {
				output = append(output, value)
			}
		}
	}
	return output
}

func MapSlices[I any, O any](fn func(I) O, slices ...[]I) []O {
	var totalLen int
	for _, slice := range slices {
		totalLen += len(slice)
	}

	output := make([]O, 0, totalLen)
	for _, slice := range slices {
		for _, v := range slice {
			output = append(output, fn(v))
		}
	}

	return output
}
