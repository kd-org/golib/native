package native

import (
	"testing"
)

func TestSet(t *testing.T) {
	set := NewSetFromSlice([]int{1, 3, 5, 2, 5, 8})
	for _, v := range []int{1, 3, 5, 2, 5, 8} {
		if !set.Has(v) {
			t.Fatalf("set %v should has %v", set, v)
		}
	}
	if set.Has(9) {
		t.Fatalf("set %v should not has %v", set, 9)
	}
	if !set.HasAll([]int{1, 3, 5, 2, 5, 8}) {
		t.Fatalf("set %v should has all %v", set, []int{1, 3, 5, 2, 5, 8})
	}
	if set.HasAll([]int{1, 3, 5, 2, 5, 8, 9}) {
		t.Fatalf("set %v should has all %v", set, []int{1, 3, 5, 2, 5, 8, 9})
	}
	if !set.HasAny([]int{1, 2, 3, 4, 5}) {
		t.Fatalf("set %v should has all %v", set, []int{1, 2, 3, 4, 5})
	}
	if set.HasAny([]int{6, 9}) {
		t.Fatalf("set %v should not has any %v", set, []int{6, 9})
	}
	set.Add(9)
	if !set.Has(9) {
		t.Fatalf("set %v should has %v", set, 9)
	}
	if len(set.Slice()) != 6 {
		t.Fatalf("set %v should has %v elements", set, 6)
	}
}
