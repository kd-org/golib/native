//go:build windows
// +build windows

package native

import "os"

func NewReloadSignal() <-chan os.Signal {
	osSignalReload := make(chan os.Signal, 1)
	return osSignalReload
}
