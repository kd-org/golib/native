//go:build linux || darwin
// +build linux darwin

package native

import (
	"os"
	"os/signal"
	"syscall"
)

func NewReloadSignal() <-chan os.Signal {
	osSignalReload := make(chan os.Signal, 1)
	signal.Notify(osSignalReload, syscall.SIGHUP, syscall.SIGUSR1)

	return osSignalReload
}
