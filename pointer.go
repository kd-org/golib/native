package native

func Pointer[T interface{}](data T) *T {
	return &data
}
